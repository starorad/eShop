package cz.cvut.eshop.archive;

import cz.cvut.eshop.shop.Item;

import java.security.InvalidParameterException;

public class ItemPurchaseArchiveEntry {
    private final Item refItem;
    private int soldCount;
    
    public ItemPurchaseArchiveEntry(Item refItem) {
        this.refItem = refItem;
        soldCount = 1;
    }
    
    void increaseCountHowManyTimesHasBeenSold(int x) throws InvalidParameterException {
        if (x <= 0) {
            throw new InvalidParameterException("Cannot increase times sold by a negative number");
        }
        soldCount += x;
    }
    
    int getCountHowManyTimesHasBeenSold() {
        return soldCount;
    }
    
    Item getRefItem() {
        return refItem;
    }
    
    @Override
    public String toString() {
        return "ITEM " + refItem.toString() + " HAS BEEN SOLD " + soldCount + " TIMES";
    }
}
