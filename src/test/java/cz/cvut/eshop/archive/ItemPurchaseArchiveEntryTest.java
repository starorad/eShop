package cz.cvut.eshop.archive;

import org.junit.Assert;
import org.junit.Test;

import java.security.InvalidParameterException;

public class ItemPurchaseArchiveEntryTest extends AbstractArchiveTest {

	@Test
	public void testGetCountHowManyTimesHasBeenSold() {
		Assert.assertEquals("Incorrect sold count in empty archive", 1, itemArchive.getCountHowManyTimesHasBeenSold());
	}

	@Test
	public void testGetRefItem() {
		Assert.assertEquals("Constructor and getter items are different", item, itemArchive.getRefItem());
	}

	@Test(expected = InvalidParameterException.class)
	public void testIncreaseCountHowManyTimesHasBeenSoldZero() {
		itemArchive.increaseCountHowManyTimesHasBeenSold(0);
	}

	@Test
	public void testIncreaseCountHowManyTimesHasBeenSoldPositive() {
		int soldCount = itemArchive.getCountHowManyTimesHasBeenSold();
		itemArchive.increaseCountHowManyTimesHasBeenSold(10);
		Assert.assertEquals("Incorrect sold count for " + 10 + " increment", soldCount + 10, itemArchive.getCountHowManyTimesHasBeenSold());
	}

	@Test(expected = InvalidParameterException.class)
	public void testIncreaseCountHowManyTimesHasBeenSoldNegative() {
		itemArchive.increaseCountHowManyTimesHasBeenSold(-10);
	}

	@Test
	public void testToStringWithDefaultValue() {
		String itemString = itemArchive.getRefItem().toString();
		String expected = "ITEM " + itemString + " HAS BEEN SOLD 1 TIMES";
		Assert.assertEquals(expected, itemArchive.toString());
	}

	@Test
	public void testToStringAfterIncrement() {
		itemArchive.increaseCountHowManyTimesHasBeenSold(5);
		String itemString = itemArchive.getRefItem().toString();
		String expected = "ITEM " + itemString + " HAS BEEN SOLD 6 TIMES";
		Assert.assertEquals(expected, itemArchive.toString());
	}
}